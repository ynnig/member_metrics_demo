# member_metrics_demo

Just testing to see if we can use Jupyter notebooks as the basis for future Crossref reports.

Click on the button below to launch this notebook in dashboard mode.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/crossref%2Fmember_metrics_demo/master?urlpath=%2Fvoila%2F--debug%2Frender%2Findex.ipynb)
