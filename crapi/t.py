import logging
import concurrent.futures
import time

from multiprocessing import Pool, Value

import requests
from tenacity import retry, stop_after_attempt, wait_random_exponential

logger = logging.getLogger(__name__)

API_BASE = "https://api.crossref.org"


@retry(stop=stop_after_attempt(5), wait=wait_random_exponential(multiplier=1, max=60))
def crapi(spec):
    query = spec["query"]
    headers = spec["headers"]

    res = requests.get(query, headers=headers)

    if res.status_code == 200:
        return res.json()["message"]

    logger.warning(f"[{res.status_code}] {query}")
    raise Exception("An HTTP error occurred")


def all_results(api=API_BASE, route="members", rows=10, headers={}):

    first_page = crapi({"query": f"{api}/{route}?rows={rows}", "headers": headers})
    total_results = first_page["total-results"]
    items = first_page["items"]

    queries = [
        {"query": f"{api}/{route}?rows={rows}&offset={offset}", "headers": headers}
        for offset in range(rows, total_results, rows)
    ]
    with Pool() as p:

        results = p.map_async(crapi, queries)
        results.wait()

    items += [item for sublist in results.get() for item in sublist["items"]]

    return total_results, items


def all_results_futures(api=API_BASE, route="members", rows=10, headers={}):

    first_page = crapi({"query": f"{api}/{route}?rows={rows}", "headers": headers})
    total_results = first_page["total-results"]
    items = first_page["items"]

    queries = [
        {"query": f"{api}/{route}?rows={rows}&offset={offset}", "headers": headers}
        for offset in range(rows, total_results, rows)
    ]

    with concurrent.futures.ThreadPoolExecutor() as executor:

        future_to_url = {
            executor.submit(crapi, spec): spec["query"] for spec in queries
        }
        for future in concurrent.futures.as_completed(future_to_url):
            url = future_to_url[future]
            try:
                data = future.result()
                items += data["items"]
            except Exception as exc:
                logger.warning("%r generated an exception: %s" % (url, exc))

    return total_results, items


if __name__ == "__main__":
    t0 = time.time()
    res1, items = all_results(rows=799)
    t1 = time.time() - t0
    print(f"res1: {t1} ({len(items)})")

    ######
    t0 = time.time()
    res2, items = all_results_futures(rows=799)
    t1 = time.time() - t0
    print(f"res2: {t1} ({len(items)})")
