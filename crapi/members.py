from statistics import mean
import shutil
import logging
from crapi.api import all_results
from diskcache import Cache, FanoutCache

logger = logging.getLogger(__name__)

cache = Cache("crapi_member_cache")

cache.stats(enable=True)

DAY = 86400
CACHE_EXPIRY = 7 * DAY


def remove_cache():
    global cache
    cache.close()
    try:
        shutil.rmtree(cache.directory)
    except OSError:  # Windows wonkiness
        pass


def cached_exists():
    global cache
    return len(cache) > 0


@cache.memoize(expire=CACHE_EXPIRY, tag="all_members")
def all_members():
    expected, items = all_results(route="members", rows=799)
    if len(items) != expected:
        logger.warning(f"Expected {expected} members, retrieved {len(items)}")

    return items


def does_nothing(member):
    if member["coverage"] == None:
        return True
    return all(count < 1 for count in member["coverage"].values())


def deadbeats(members):
    return [member for member in members if does_nothing(member)]
