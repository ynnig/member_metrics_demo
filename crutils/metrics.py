import time
from crapi import all_members, cached_exists, remove_cache, total_dois

import pandas as pd
import requests

import datetime


TOP = 10

SYNTHETIC_SUFFIX = "impact"
SYNTHETIC_COLUMNS = ["overall-coverage", f"overall-{SYNTHETIC_SUFFIX}"]

COUNT_COLUMNS = ["total-dois", "current-dois", "backfile-dois"]
YEAR_COLUMNS = ["first-deposit-year", "last-deposit-year"]
COVERAGE_PERCENT_COLUMNS = [
    "affiliations-current",
    "similarity-checking-current",
    "funders-backfile",
    "licenses-backfile",
    "funders-current",
    "affiliations-backfile",
    "resource-links-backfile",
    "orcids-backfile",
    "update-policies-current",
    "open-references-backfile",
    "orcids-current",
    "similarity-checking-backfile",
    "references-backfile",
    "award-numbers-backfile",
    "update-policies-backfile",
    "licenses-current",
    "award-numbers-current",
    "abstracts-backfile",
    "resource-links-current",
    "abstracts-current",
    "open-references-current",
    "references-current",
]
COVERAGE_TOTAL_COLUMNS = [f"{name}-total" for name in COVERAGE_PERCENT_COLUMNS]
COVERAGE_COLUMNS = sorted(COVERAGE_PERCENT_COLUMNS + COVERAGE_TOTAL_COLUMNS)
BASE_COLUMNS = ["primary-name", "id"] + SYNTHETIC_COLUMNS
CHECK_COLUMN = ["record-check"]

COLUMN_ORDER = (
    BASE_COLUMNS + YEAR_COLUMNS + COUNT_COLUMNS + COVERAGE_COLUMNS + CHECK_COLUMN
)

DISPLAY_COLUMNS = [
    "primary-name",
    "overall-coverage",
    f"overall-{SYNTHETIC_SUFFIX}",
    "first-deposit-year",
    "last-deposit-year",
    "total-dois",
    "current-dois",
    "backfile-dois",
]

summary_df = None


TOTAL_CROSSREF_DOIS = total_dois()


def get_board_members():
    return pd.read_csv("board_ids.csv")


def convert_to_int(df, columns):
    for column in columns:
        if column in df:
            df[column] = df[column].astype("Int64")
    return df


def calculate_active_years(member: dict) -> tuple:
    """ extract all the years from breakdowns, return tuple of first & last years """
    publishing_years = [
        entry[0]
        for entry in member["breakdowns"]["dois-by-issued-year"]
        if entry[1] > 0
    ]
    # If we see no activity, set activity to the future (next year)
    if len(publishing_years) == 0:
        publishing_years = [datetime.datetime.today().year + 1]
    return (min(publishing_years), max(publishing_years))


def summarize(member: dict) -> dict:
    # API returns some broken member records
    # we need to flag those that have null counts
    sane = True if member["counts"] else False

    # The minimal data
    summary = {
        "id": member["id"],
        "record-check": sane,
        "primary-name": member["primary-name"],
    }

    if sane:
        # Get the rest of the data
        summary.update(member["coverage"])
        summary.update(member["counts"])
        first_deposit_year, last_deposit_year = calculate_active_years(member)
        summary.update(
            {
                "first-deposit-year": first_deposit_year,
                "last-deposit-year": last_deposit_year,
            }
        )

    return summary


def cleanup_dataframe(df):
    # fill missing values with zero
    df = df.fillna(0)
    # adjust ridiculous precision
    # df = df.round(2) TODO
    # show ints as ints
    df = convert_to_int(df, COUNT_COLUMNS + YEAR_COLUMNS)
    return df


def add_overall_coverage(df):
    coverage_df = df[COVERAGE_PERCENT_COLUMNS]
    df["overall-coverage"] = coverage_df.mean(axis=1)
    # df[f"overall-{SYNTHETIC_SUFFIX}"] = round(
    #     (df["overall-coverage"] * df["total-dois"]) / TOTAL_CROSSREF_DOIS, 4
    # ) TODO
    df[f"overall-{SYNTHETIC_SUFFIX}"] = (
        df["overall-coverage"] * df["total-dois"]
    ) / TOTAL_CROSSREF_DOIS

    return df


def add_estimated_totals(df):
    for column_name in COVERAGE_PERCENT_COLUMNS:
        metric_name, period = column_name.rsplit("-", 1)
        period_total = f"{period}-dois"
        subtotal_name = f"{column_name}-total"
        # df[subtotal_name] = df[column_name].round(2) * df[period_total] TODO
        df[subtotal_name] = df[column_name] * df[period_total]

        df[subtotal_name] = df[subtotal_name].astype("int64")
    return df


def format_as_percentage(df, c):
    df["overall-coverage"] = pd.Series(
        ["{0:.2f}%".format(val) for val in df["overall-coverage"]], index=df.index
    )
    df["overall-impact"] = pd.Series(
        ["{0:.2f}%".format(val) for val in df["overall-impact"]], index=df.index
    )
    # TODO fix so not hard-coded. But code below is giving me angina.
    # top_by_coverage['overall-impact'] = pd.Series(["{0:.2f}%".format(val * 100) for val in top_by_coverage['overall-impact']], index = top_by_coverage.index)
    # for column in columns:
    #     df[column] = pd.Series(
    #         ["{0:.2f}%".format(val) for val in df[column]], index=df.index
    #     )

    return df


def get_metrics():

    members = all_members()

    summary = [summarize(member_record) for member_record in members]

    df = pd.DataFrame(summary)
    df = df.reindex(COLUMN_ORDER, axis=1)

    df = cleanup_dataframe(df)
    df = add_overall_coverage(df)
    df = add_estimated_totals(df)
    # df = format_as_percentage(df, ["overall-coverage", "overall-impact"])

    return df
