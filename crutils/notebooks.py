from IPython.display import HTML
import base64
import pandas as pd
import io


def toggle_code():
    return HTML(
        """<script>
    code_show=true; 
    function code_toggle() {
     if (code_show){
     $('div.input').hide();
     } else {
     $('div.input').show();
     }
     code_show = !code_show
    } 
    $( document ).ready(code_toggle);
    </script>
    <form action="javascript:code_toggle()"><input type="submit" value="Click here to toggle on/off the raw code."></form>"""
    )


def create_excel_download_link(df, title="Download Excel file", filename="data.xlsx"):
    output = io.BytesIO()
    writer = pd.ExcelWriter(output, engine="xlsxwriter")

    excel = df.to_excel(writer, sheet_name="results", index=False)
    writer.save()
    xlsx_data = output.getvalue()
    b64 = base64.b64encode(xlsx_data)
    payload = b64.decode()
    html = '<a download="{filename}" href="data:text/xlsx;base64,{payload}" target="_blank">{title}</a>'
    html = html.format(payload=payload, title=title, filename=filename)
    return HTML(html)


def create_csv_download_link(df, title="Download CSV file", filename="data.csv"):
    csv = df.to_csv()
    b64 = base64.b64encode(csv.encode())
    payload = b64.decode()
    html = '<a download="{filename}" href="data:text/csv;base64,{payload}" target="_blank">{title}</a>'
    html = html.format(payload=payload, title=title, filename=filename)
    return HTML(html)


def create_json_download_link(
    json_data, title="Download JSON file", filename="data.json"
):
    b64 = base64.b64encode(json.dumps(json_data).encode())
    payload = b64.decode()
    html = '<a download="{filename}" href="data:text/json;charset=utf-8,{payload}" target="_blank">{title}</a>'
    html = html.format(payload=payload, title=title, filename=filename)
    return HTML(html)
